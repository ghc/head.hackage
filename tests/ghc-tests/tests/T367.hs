{-# LANGUAGE MagicHash #-}
{-# OPTIONS_GHC  -O2 #-}
module T367 where
import Control.Concurrent
import qualified Data.Vector as U
import System.IO

main stdout _ = do
    -- Non allocating loop, needs -fno-omit-yields in order for the kill to
    -- work
    t <- forkIO (U.sum (U.enumFromTo 1 (1000000000 :: Int)) `seq` return ())
    threadDelay 10
    killThread t
    hPutStrLn stdout "Done"
