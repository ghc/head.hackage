{-# OPTIONS_GHC -O2 -fno-omit-yields #-}
-- Without -fno-omit-yields this test doesn't terminate
module T367A where

import Data.IORef
import Control.Concurrent
import System.IO

main :: Handle -> Handle -> IO ()
main stdout _ = do
    r <- newIORef False
    done_var <- newEmptyMVar
    hPutStrLn stdout "About to fork"

    forkIO $ f stdout done_var r
    threadDelay 1000000 -- 1 second

    hPutStrLn stdout "Why is this never printed?!"
    writeIORef r True
    readMVar done_var
    -- and why do we never exit?

f :: Handle -> MVar () -> IORef Bool -> IO ()
f stdout done_var r = readIORef r >>= \b-> if b then hPutStrLn stdout "Done" >> putMVar done_var ()  else f stdout done_var r
