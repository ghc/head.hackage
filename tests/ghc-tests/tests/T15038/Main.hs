module T15038.Main where
import qualified T15038.Parser as Parser
import System.IO

main :: Handle -> Handle -> IO ()
main stdout _ = hPrint stdout (iterate Parser.byteParserBadOnce 5 !! 100000)
